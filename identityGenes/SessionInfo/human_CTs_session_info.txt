R version 4.0.3 (2020-10-10)
Platform: x86_64-conda-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /mnt/irisgpfs/users/cbarlier/libraries/miniconda3/envs/r4-base-clone1/lib/libopenblasp-r0.3.12.so

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] parallel  stats4    stats     graphics  grDevices utils     datasets 
[8] methods   base     

other attached packages:
[1] org.Hs.eg.db_3.12.0  AnnotationDbi_1.52.0 IRanges_2.24.1      
[4] S4Vectors_0.28.1     Biobase_2.50.0       BiocGenerics_0.36.1 
[7] Matrix_1.3-4         stringr_1.4.0        data.table_1.14.0   

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.7      lattice_0.20-41 grid_4.0.3      DBI_1.1.1      
 [5] magrittr_2.0.1  RSQLite_2.2.7   cachem_1.0.5    rlang_0.4.11   
 [9] stringi_1.6.2   blob_1.2.1      vctrs_0.3.8     tools_4.0.3    
[13] bit64_4.0.5     bit_4.0.4       fastmap_1.1.0   compiler_4.0.3 
[17] pkgconfig_2.0.3 memoise_2.0.0  
