options(stringsAsFactors = F)
require(Matrix)
require(org.Hs.eg.db) #queried on the 14/03/2022
library(stringr)

pathOut = "/scratch/users/cbarlier/scGLQ/ANALYSES/ENRICHMENT/"

########################### MOUSE ############################

#Load the Tabula Muris data
tbm <- readRDS("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/manualCuration_TBM.rds")
tbm_mtx <- tbm$counts
rm(tbm)

#Retrieve genes universe
mm_uniGenes <- data.frame("Gene"=dimnames(tbm_mtx)[[1]])

#Save
saveRDS(mm_uniGenes,paste0(pathOut,"mouse_genes_universe.rds"))

#Clean
rm(tbm_mtx)
rm(mm_uniGenes)

##############################################################

########################### HUMAN ############################

#Load the Tabula Sapiens data
ts <- readRDS("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/DATA/tabula_sapiens/UMI10x_downsampled_tabula_sapiens.rds")
ts_mtx <- ts$mtx
rm(ts)

#Retrieve genes universe
hg_uniGenes <- data.frame("Gene"=dimnames(ts_mtx)[[1]])

#Get Gene Symbol equivalent
EnsemblIDs <- unique(hg_uniGenes$Gene)
dfCoresp <- select(org.Hs.eg.db, keys = EnsemblIDs, keytype = 'ENSEMBL',columns = c('SYMBOL', 'ENSEMBL'))
#Remove NAs
dfCoresp <- dfCoresp[!is.na(dfCoresp$SYMBOL),]
dfCoresp <- dfCoresp[!is.na(dfCoresp$ENSEMBL),]
#Add info
colnames(hg_uniGenes) <- c("ENSEMBL")
hg_uniGenes <- merge(hg_uniGenes,dfCoresp,by="ENSEMBL",all.x=T)
colnames(hg_uniGenes) <- c("EnsemblID","GeneSymbol")

#Save
saveRDS(hg_uniGenes,paste0(pathOut,"human_genes_universe.rds"))

#Save session info - HPC r4-base-clone1
sessionInfo() %>% capture.output(file=paste0(pathOut,"session_info.txt"))

#Clean
rm(ts_mtx)
rm(EnsemblIDs)
rm(dfCoresp)
rm(hg_uniGenes)
##############################################################