tissue	cell_type	cell_ontology_class	CT	subCT	
pancreas	pancreatic_acinar_cell	pancreatic_acinar_cell	acinar_cell	NA	
saliva-secreting_gland	acinar_cell_of_salivary_gland	acinar_cell_of_salivary_gland	acinar_cell	NA	
eye	fat_cell	adipocyte	adipocyte	NA	
lung	capillary_endothelial_cell	capillary_aerocyte	aerocyte	NA	
pancreas	pancreatic_A_cell	pancreatic_alpha_cell	alpha_cell	NA	
adipose_tissue	plasma_cell	plasma_cell	B_cell	plasma_cell	
bladder_organ	plasma_cell	plasma_cell	B_cell	plasma_cell	
bladder_organ	B_cell	b_cell	B_cell	NA	
blood	naive_B_cell	naive_b_cell	B_cell	naive_b_cell	
blood	memory_B_cell	memory_b_cell	B_cell	memory_b_cell	
blood	plasma_cell	plasma_cell	B_cell	plasma_cell	
blood	plasmablast	plasmablast	B_cell	plasmablast	
bone_marrow	plasma_cell	plasma_cell	B_cell	plasma_cell	
bone_marrow	naive_B_cell	naive_b_cell	B_cell	naive_b_cell	
bone_marrow	memory_B_cell	memory_b_cell	B_cell	memory_b_cell	
eye	B_cell	b_cell	B_cell	NA	
eye	plasma_cell	plasma_cell	B_cell	plasma_cell	
kidney	B_cell	b_cell	B_cell	NA	
large_intestine	plasma_cell	plasma_cell	B_cell	plasma_cell	
large_intestine	B_cell	b_cell	B_cell	NA	
liver	plasma_cell	plasma_cell	B_cell	plasma_cell	
lung	plasma_cell	plasma_cell	B_cell	plasma_cell	
lung	B_cell	b_cell	B_cell	NA	
lymph_node	B_cell	b_cell	B_cell	NA	
lymph_node	plasma_cell	plasma_cell	B_cell	plasma_cell	
lymph_node	naive_B_cell	naive_b_cell	B_cell	naive_b_cell	
lymph_node	memory_B_cell	memory_b_cell	B_cell	memory_b_cell	
mammary_gland	B_cell	b_cell	B_cell	NA	
mammary_gland	plasma_cell	plasma_cell	B_cell	plasma_cell	
pancreas	B_cell	b_cell	B_cell	NA	
pancreas	plasma_cell	plasma_cell	B_cell	plasma_cell	
saliva-secreting_gland	naive_B_cell	naive_b_cell	B_cell	naive_b_cell	
saliva-secreting_gland	memory_B_cell	memory_b_cell	B_cell	memory_b_cell	
saliva-secreting_gland	B_cell	b_cell	B_cell	NA	
saliva-secreting_gland	plasma_cell	plasma_cell	B_cell	plasma_cell	
skin_of_body	plasma_cell	plasma_cell	B_cell	plasma_cell	
skin_of_body	memory_B_cell	memory_b_cell	B_cell	memory_b_cell	
skin_of_body	naive_B_cell	naive_b_cell	B_cell	naive_b_cell	
small_intestine	B_cell	b_cell	B_cell	NA	
small_intestine	plasma_cell	plasma_cell	B_cell	plasma_cell	
spleen	memory_B_cell	memory_b_cell	B_cell	memory_b_cell	
spleen	naive_B_cell	naive_b_cell	B_cell	naive_b_cell	
spleen	plasma_cell	plasma_cell	B_cell	plasma_cell	
thymus	B_cell	b_cell	B_cell	NA	
thymus	plasma_cell	plasma_cell	B_cell	plasma_cell	
thymus	memory_B_cell	memory_b_cell	B_cell	memory_b_cell	
thymus	naive_B_cell	naive_b_cell	B_cell	naive_b_cell	
trachea	plasma_cell	plasma_cell	B_cell	plasma_cell	
trachea	B_cell	b_cell	B_cell	NA	
uterus	B_cell	b_cell	B_cell	NA	
vasculature	B_cell	b_cell	B_cell	NA	
vasculature	plasma_cell	plasma_cell	B_cell	plasma_cell	
lung	basal_cell	basal_cell	basal_cell	NA	
mammary_gland	basal_cell	basal_cell	basal_cell	NA	
prostate_gland	basal_cell_of_prostate_epithelium	basal_cell_of_prostate_epithelium	basal_cell	NA	
saliva-secreting_gland	basal_cell	basal_cell	basal_cell	NA	
tongue	basal_cell	basal_cell	basal_cell	NA	
trachea	basal_cell	basal_cell	basal_cell	NA	
blood	basophil	basophil	basophil	NA	
lung	basophil	basophil	basophil	NA	
pancreas	type_B_pancreatic_cell	pancreatic_beta_cell	beta_cell	NA	
liver	intrahepatic_cholangiocyte	intrahepatic_cholangiocyte	cholangiocyte	NA	
lung	club_cell	club_cell	club_cell	NA	
prostate_gland	club_cell	hillock-club_cell_of_prostate_epithelium	club_cell	hillock_cell	
prostate_gland	club_cell	club_cell_of_prostate_epithelium	club_cell	NA	
blood	common_myeloid_progenitor	myeloid_progenitor	common_myeloid_progenitor	NA	
bone_marrow	common_myeloid_progenitor	myeloid_progenitor	common_myeloid_progenitor	NA	
pancreas	pancreatic_D_cell	pancreatic_delta_cell	delta_cell	NA	
bladder_organ	plasmacytoid_dendritic_cell	plasmacytoid_dendritic_cell	dendritic_cell	plasmacytoid_dendritic_cell	
blood	CD141-positive_myeloid_dendritic_cell	cd141-positive_myeloid_dendritic_cell	dendritic_cell	CD141_myeloid_dendritic_cell	
blood	plasmacytoid_dendritic_cell	plasmacytoid_dendritic_cell	dendritic_cell	plasmacytoid_dendritic_cell	
eye	dendritic_cell	dendritic_cell	dendritic_cell	NA	
liver	liver_dendritic_cell	liver_dendritic_cell	dendritic_cell	NA	
lung	dendritic_cell	dendritic_cell	dendritic_cell	NA	
lung	plasmacytoid_dendritic_cell	plasmacytoid_dendritic_cell	dendritic_cell	plasmacytoid_dendritic_cell	
lymph_node	CD141-positive_myeloid_dendritic_cell	cd141-positive_myeloid_dendritic_cell	dendritic_cell	CD141_myeloid_dendritic_cell	
lymph_node	CD1c-positive_myeloid_dendritic_cell	cd1c-positive_myeloid_dendritic_cell	dendritic_cell	CD1C_myeloid_dendritic_cell	
lymph_node	plasmacytoid_dendritic_cell	plasmacytoid_dendritic_cell	dendritic_cell	plasmacytoid_dendritic_cell	
lymph_node	mature_conventional_dendritic_cell	mature_conventional_dendritic_cell	dendritic_cell	mature_conventional_dendritic_cell	
skin_of_body	CD141-positive_myeloid_dendritic_cell	cd141-positive_myeloid_dendritic_cell	dendritic_cell	CD141_myeloid_dendritic_cell	
skin_of_body	CD1c-positive_myeloid_dendritic_cell	cd1c-positive_myeloid_dendritic_cell	dendritic_cell	CD1C_myeloid_dendritic_cell	
spleen	CD141-positive_myeloid_dendritic_cell	cd141-positive_myeloid_dendritic_cell	dendritic_cell	CD141_myeloid_dendritic_cell	
spleen	CD1c-positive_myeloid_dendritic_cell	cd1c-positive_myeloid_dendritic_cell	dendritic_cell	CD1C_myeloid_dendritic_cell	
spleen	plasmacytoid_dendritic_cell	plasmacytoid_dendritic_cell	dendritic_cell	plasmacytoid_dendritic_cell	
thymus	myeloid_dendritic_cell	myeloid_dendritic_cell	dendritic_cell	myeloid_dendritic_cell	
thymus	dendritic_cell	dendritic_cell	dendritic_cell	NA	
pancreas	pancreatic_ductal_cell	pancreatic_ductal_cell	ductal_cell	NA	
adipose_tissue	endothelial_cell	endothelial_cell	endothelial_cell	NA	
bladder_organ	capillary_endothelial_cell	capillary_endothelial_cell	endothelial_cell	capillary_endothelial_cell	
bladder_organ	vein_endothelial_cell	vein_endothelial_cell	endothelial_cell	vein_endothelial_cell	
bladder_organ	endothelial_cell_of_lymphatic_vessel	endothelial_cell_of_lymphatic_vessel	endothelial_cell	lymphatic_endothelial	
eye	retinal_blood_vessel_endothelial_cell	retinal_blood_vessel_endothelial_cell	endothelial_cell	blood_vessel_endothelial_cell	
eye	endothelial_cell	endothelial_cell	endothelial_cell	NA	
heart	cardiac_endothelial_cell	cardiac_endothelial_cell	endothelial_cell	cardiac_endothelial_cell	
kidney	endothelial_cell	endothelial_cell	endothelial_cell	NA	
large_intestine	gut_endothelial_cell	gut_endothelial_cell	endothelial_cell	gut_endothelial_cell	
liver	endothelial_cell_of_hepatic_sinusoid	endothelial_cell_of_hepatic_sinusoid	endothelial_cell	endothelial_cell_of_hepatic_sinusoid	
liver	endothelial_cell	endothelial_cell	endothelial_cell	NA	
lung	blood_vessel_endothelial_cell	bronchial_vessel_endothelial_cell	endothelial_cell	bronchial_vessel_endothelial_cell	
lung	endothelial_cell_of_artery	endothelial_cell_of_artery	endothelial_cell	endothelial_cell_of_artery	
lung	capillary_endothelial_cell	capillary_endothelial_cell	endothelial_cell	capillary_endothelial_cell	
lung	vein_endothelial_cell	vein_endothelial_cell	endothelial_cell	vein_endothelial_cell	
lung	endothelial_cell_of_lymphatic_vessel	endothelial_cell_of_lymphatic_vessel	endothelial_cell	lymphatic_endothelial	
lung	lung_microvascular_endothelial_cell	lung_microvascular_endothelial_cell	endothelial_cell	microvascular_endothelial_cell	
lymph_node	endothelial_cell	endothelial_cell	endothelial_cell	NA	
mammary_gland	vein_endothelial_cell	vein_endothelial_cell	endothelial_cell	vein_endothelial_cell	
mammary_gland	endothelial_cell_of_artery	endothelial_cell_of_artery	endothelial_cell	endothelial_cell_of_artery	
mammary_gland	endothelial_cell_of_lymphatic_vessel	endothelial_cell_of_lymphatic_vessel	endothelial_cell	lymphatic_endothelial	
muscle_tissue	capillary_endothelial_cell	capillary_endothelial_cell	endothelial_cell	capillary_endothelial_cell	
muscle_tissue	endothelial_cell_of_vascular_tree	endothelial_cell_of_vascular_tree	endothelial_cell	endothelial_cell_of_vascular_tree	
muscle_tissue	endothelial_cell_of_artery	endothelial_cell_of_artery	endothelial_cell	endothelial_cell_of_artery	
muscle_tissue	endothelial_cell_of_lymphatic_vessel	endothelial_cell_of_lymphatic_vessel	endothelial_cell	lymphatic_endothelial	
pancreas	endothelial_cell	endothelial_cell	endothelial_cell	NA	
prostate_gland	endothelial_cell	endothelial_cell	endothelial_cell	NA	
saliva-secreting_gland	endothelial_cell_of_lymphatic_vessel	endothelial_cell_of_lymphatic_vessel	endothelial_cell	lymphatic_endothelial	
saliva-secreting_gland	endothelial_cell	endothelial_cell	endothelial_cell	NA	
skin_of_body	endothelial_cell	endothelial_cell	endothelial_cell	NA	
small_intestine	gut_endothelial_cell	gut_endothelial_cell	endothelial_cell	gut_endothelial_cell	
spleen	endothelial_cell	endothelial_cell	endothelial_cell	NA	
thymus	vein_endothelial_cell	vein_endothelial_cell	endothelial_cell	vein_endothelial_cell	
thymus	capillary_endothelial_cell	capillary_endothelial_cell	endothelial_cell	capillary_endothelial_cell	
thymus	endothelial_cell_of_artery	endothelial_cell_of_artery	endothelial_cell	endothelial_cell_of_artery	
thymus	endothelial_cell_of_lymphatic_vessel	endothelial_cell_of_lymphatic_vessel	endothelial_cell	lymphatic_endothelial	
tongue	vein_endothelial_cell	vein_endothelial_cell	endothelial_cell	vein_endothelial_cell	
tongue	endothelial_cell_of_lymphatic_vessel	endothelial_cell_of_lymphatic_vessel	endothelial_cell	lymphatic_endothelial	
tongue	endothelial_cell_of_artery	endothelial_cell_of_artery	endothelial_cell	endothelial_cell_of_artery	
tongue	capillary_endothelial_cell	capillary_endothelial_cell	endothelial_cell	capillary_endothelial_cell	
trachea	endothelial_cell	endothelial_cell	endothelial_cell	NA	
uterus	endothelial_cell	endothelial_cell	endothelial_cell	NA	
uterus	endothelial_cell_of_lymphatic_vessel	endothelial_cell_of_lymphatic_vessel	endothelial_cell	lymphatic_endothelial	
vasculature	capillary_endothelial_cell	vein_or_capillary_endothelial_cell	endothelial_cell	NA	
vasculature	endothelial_cell_of_artery	endothelial_cell_of_artery	endothelial_cell	endothelial_cell_of_artery	
vasculature	endothelial_cell_of_lymphatic_vessel	lymphatic_endothelial_cells	endothelial_cell	lymphatic_endothelial	
vasculature	endothelial_cell	endothelial_cell	endothelial_cell	NA	
large_intestine	enterocyte_of_epithelium_of_large_intestine	enterocyte_of_epithelium_of_large_intestine	enterocyte	NA	
large_intestine	enterocyte	immature_enterocyte	enterocyte	immature_enterocyte	
large_intestine	enterocyte	mature_enterocyte	enterocyte	mature_enterocyte	
small_intestine	enterocyte_of_epithelium_of_small_intestine	enterocyte_of_epithelium_of_small_intestine	enterocyte	NA	
small_intestine	enterocyte	immature_enterocyte	enterocyte	immature_enterocyte	
small_intestine	enterocyte	mature_enterocyte	enterocyte	mature_enterocyte	
large_intestine	intestinal_enteroendocrine_cell	intestinal_enteroendocrine_cell	enteroendocrine_cell	NA	
small_intestine	intestinal_enteroendocrine_cell	intestinal_enteroendocrine_cell	enteroendocrine_cell	NA	
eye	conjunctival_epithelial_cell	conjunctival_epithelial_cell	epithelial_cell	conjunctival_epithelial_cell	
eye	epithelial_cell_of_lacrimal_sac	epithelial_cell_of_lacrimal_sac	epithelial_cell	epithelial_cell_of_lacrimal_sac	
eye	retinal_pigment_epithelial_cell	retinal_pigment_epithelial_cell	epithelial_cell	retinal_pigment_epithelial_cell	
eye	corneal_epithelial_cell	corneal_epithelial_cell	epithelial_cell	corneal_epithelial_cell	
eye	pigmented_ciliary_epithelial_cell	ciliary_body	epithelial_cell	pigmented_ciliary_epithelial_cell	
kidney	kidney_epithelial_cell	kidney_epithelial_cell	epithelial_cell	NA	
lung	lung_ciliated_cell	lung_ciliated_cell	ciliated_cell	NA	
prostate_gland	epithelial_cell	epithelial_cell	epithelial_cell	NA	
saliva-secreting_gland	duct_epithelial_cell	duct_epithelial_cell	epithelial_cell	duct_epithelial_cell	
saliva-secreting_gland	myoepithelial_cell	myoepithelial_cell	myoepithelial_cell	NA	
skin_of_body	epithelial_cell	epithelial_cell	epithelial_cell	NA	
thymus	medullary_thymic_epithelial_cell	medullary_thymic_epithelial_cell	epithelial_cell	medullary_thymic_epithelial_cell	
tongue	epithelial_cell	epithelial_cell	epithelial_cell	NA	
trachea	ciliated_cell	ciliated_cell	ciliated_cell	NA	
uterus	epithelial_cell_of_uterus	epithelial_cell_of_uterus	epithelial_cell	NA	
uterus	epithelial_cell	epithelial_cell	epithelial_cell	NA	
uterus	ciliated_epithelial_cell	ciliated_epithelial_cell	ciliated_cell	NA	
vasculature	epithelial_cell	epithelial_cell	epithelial_cell	NA	
blood	erythrocyte	erythrocyte	erythrocyte	NA	
bone_marrow	erythrocyte	erythrocyte	erythrocyte	NA	
liver	erythrocyte	erythrocyte	erythrocyte	NA	
lymph_node	erythrocyte	erythrocyte	erythrocyte	NA	
muscle_tissue	erythrocyte	erythrocyte	erythrocyte	NA	
spleen	erythrocyte	erythrocyte	erythrocyte	NA	
thymus	erythrocyte	erythrocyte	erythrocyte	NA	
vasculature	erythrocyte	erythrocyte	erythrocyte	NA	
prostate_gland	erythroid_progenitor_cell	erythroid_progenitor	erythroid_progenitor_cell	NA	
bone_marrow	erythroid_progenitor_cell	erythroid_progenitor	erythroid_progenitor_cell	NA	
eye	eye_photoreceptor_cell	eye_photoreceptor_cell	eye_photoreceptor_cell	NA	
eye	keratocyte	corneal_keratocyte	fibroblast	keratocyte	
adipose_tissue	fibroblast	fibroblast	fibroblast	NA	
adipose_tissue	myofibroblast_cell	myofibroblast_cell	fibroblast	myofibroblast_cell	
bladder_organ	fibroblast	fibroblast	fibroblast	NA	
bladder_organ	myofibroblast_cell	myofibroblast_cell	fibroblast	myofibroblast_cell	
eye	fibroblast	fibroblast	fibroblast	NA	
heart	fibroblast_of_cardiac_tissue	fibroblast_of_cardiac_tissue	fibroblast	NA	
large_intestine	fibroblast	fibroblast	fibroblast	NA	
liver	fibroblast	fibroblast	fibroblast	NA	
lung	fibroblast	alveolar_fibroblast	fibroblast	alveolar_fibroblast	
lung	myofibroblast_cell	myofibroblast_cell	fibroblast	myofibroblast_cell	
lung	fibroblast	fibroblast	fibroblast	NA	
mammary_gland	fibroblast_of_breast	fibroblast_of_breast	fibroblast	NA	
pancreas	pancreatic_stellate_cell	pancreatic_stellate_cell	pancreatic_stellate_cell	NA	
pancreas	fibroblast	fibroblast	fibroblast	NA	
prostate_gland	fibroblast	fibroblast	fibroblast	NA	
saliva-secreting_gland	fibroblast	fibroblast	fibroblast	NA	
small_intestine	fibroblast	fibroblast	fibroblast	NA	
thymus	fibroblast	fibroblast	fibroblast	NA	
tongue	fibroblast	fibroblast	fibroblast	NA	
trachea	fibroblast	fibroblast	fibroblast	NA	
uterus	fibroblast	fibroblast	fibroblast	NA	
vasculature	fibroblast	fibroblast	fibroblast	NA	
muscle_tissue	tendon_cell	tendon_cell	tendon_cell	NA	
small_intestine	duodenum_glandular_cell	duodenum_glandular_cell	glandular_cell	NA	
eye	microglial_cell	microglial_cell	microglial_cell	NA	
eye	Muller_cell	muller_cell	Muller_cell	NA	
eye	radial_glial_cell	radial_glial_cell	radial_glial_cell	NA	
tongue	Schwann_cell	schwann_cell	schwann_cell	NA	
large_intestine	large_intestine_goblet_cell	large_intestine_goblet_cell	goblet_cell	NA	
large_intestine	goblet_cell	goblet_cell	goblet_cell	NA	
lung	respiratory_goblet_cell	respiratory_mucous_cell	goblet_cell	respiratory_goblet_cell	
lung	respiratory_goblet_cell	respiratory_goblet_cell	goblet_cell	respiratory_goblet_cell	
small_intestine	small_intestine_goblet_cell	small_intestine_goblet_cell	goblet_cell	NA	
small_intestine	goblet_cell	goblet_cell	goblet_cell	NA	
trachea	tracheal_goblet_cell	tracheal_goblet_cell	goblet_cell	NA	
trachea	goblet_cell	goblet_cell	goblet_cell	NA	
blood	granulocyte	granulocyte	granulocyte	NA	
bone_marrow	granulocyte	granulocyte	granulocyte	NA	
blood	hematopoietic_stem_cell	hematopoietic_stem_cell	hematopoietic_stem_cell	NA	
bone_marrow	hematopoietic_stem_cell	hematopoietic_stem_cell	hematopoietic_stem_cell	NA	
lymph_node	hematopoietic_stem_cell	hematopoietic_stem_cell	hematopoietic_stem_cell	NA	
spleen	hematopoietic_stem_cell	hematopoietic_stem_cell	hematopoietic_stem_cell	NA	
liver	hepatocyte	hepatocyte	hepatocyte	NA	
prostate_gland	epithelial_cell	hillock_cell_of_prostate_epithelium	club_cell	hillock_cell	
eye	retina_horizontal_cell	retina_horizontal_cell	horizontal_cell	retina_horizontal_cell	
large_intestine	intestinal_crypt_stem_cell_of_large_intestine	intestinal_crypt_stem_cell_of_large_intestine	intestinal_crypt_stem_cell	NA	
large_intestine	intestinal_crypt_stem_cell	intestinal_crypt_stem_cell	intestinal_crypt_stem_cell	NA	
small_intestine	intestinal_crypt_stem_cell_of_small_intestine	intestinal_crypt_stem_cell_of_small_intestine	intestinal_crypt_stem_cell	NA	
small_intestine	intestinal_crypt_stem_cell	intestinal_crypt_stem_cell	intestinal_crypt_stem_cell	NA	
lung	pulmonary_ionocyte	pulmonary_ionocyte	ionocyte	NA	
saliva-secreting_gland	ionocyte	ionocyte	ionocyte	NA	
trachea	ionocyte	ionocyte	ionocyte	NA	
tongue	keratinocyte	keratinocyte	keratinocyte	NA	
eye	stem_cell	limbal_stem_cell	limbal_stem_cell	NA	
eye	stromal_cell	limbal_stromal_cell	mesenchymal_stem_cell	limbal_stromal_cell	
mammary_gland	luminal_epithelial_cell_of_mammary_gland	luminal_epithelial_cell_of_mammary_gland	luminal_epithelial_cell	NA	
prostate_gland	luminal_cell_of_prostate_epithelium	luminal_cell_of_prostate_epithelium	luminal_epithelial_cell	NA	
skin_of_body	Langerhans_cell	langerhans_cell	macrophage	Langerhans_cell	
adipose_tissue	macrophage	macrophage	macrophage	NA	
bladder_organ	macrophage	macrophage	macrophage	NA	
blood	macrophage	macrophage	macrophage	NA	
bone_marrow	macrophage	macrophage	macrophage	NA	
eye	macrophage	macrophage	macrophage	NA	
heart	macrophage	macrophage	macrophage	NA	
kidney	macrophage	macrophage	macrophage	NA	
liver	macrophage	macrophage	macrophage	NA	
lung	macrophage	macrophage	macrophage	NA	
lymph_node	macrophage	macrophage	macrophage	NA	
mammary_gland	macrophage	macrophage	macrophage	NA	
muscle_tissue	macrophage	macrophage	macrophage	NA	
prostate_gland	macrophage	macrophage	macrophage	NA	
saliva-secreting_gland	macrophage	macrophage	macrophage	NA	
skin_of_body	macrophage	macrophage	macrophage	NA	
spleen	macrophage	macrophage	macrophage	NA	
thymus	macrophage	macrophage	macrophage	NA	
trachea	macrophage	macrophage	macrophage	NA	
uterus	macrophage	macrophage	macrophage	NA	
vasculature	macrophage	macrophage	macrophage	NA	
adipose_tissue	mast_cell	mast_cell	mast_cell	NA	
bladder_organ	mast_cell	mast_cell	mast_cell	NA	
eye	mast_cell	mast_cell	mast_cell	NA	
large_intestine	mast_cell	mast_cell	mast_cell	NA	
lymph_node	mast_cell	mast_cell	mast_cell	NA	
mammary_gland	mast_cell	mast_cell	mast_cell	NA	
muscle_tissue	mast_cell	mast_cell	mast_cell	NA	
pancreas	mast_cell	mast_cell	mast_cell	NA	
prostate_gland	mast_cell	mast_cell	mast_cell	NA	
skin_of_body	mast_cell	mast_cell	mast_cell	NA	
small_intestine	mast_cell	mast_cell	mast_cell	NA	
thymus	mast_cell	mast_cell	mast_cell	NA	
trachea	mast_cell	mast_cell	mast_cell	NA	
vasculature	mast_cell	mast_cell	mast_cell	NA	
eye	melanocyte	melanocyte	melanocyte	NA	
skin_of_body	melanocyte	melanocyte	melanocyte	NA	
adipose_tissue	mesenchymal_stem_cell	mesenchymal_stem_cell	mesenchymal_stem_cell	NA	
muscle_tissue	mesenchymal_stem_cell	mesenchymal_stem_cell	mesenchymal_stem_cell	NA	
lung	mesothelial_cell	mesothelial_cell	mesothelial_cell	NA	
muscle_tissue	mesothelial_cell	mesothelial_cell	mesothelial_cell	NA	
thymus	mesothelial_cell	mesothelial_cell	mesothelial_cell	NA	
blood	classical_monocyte	classical_monocyte	monocyte	classical_monocyte	
blood	non-classical_monocyte	non-classical_monocyte	monocyte	non-classical_monocyte	
blood	monocyte	monocyte	monocyte	NA	
bone_marrow	monocyte	monocyte	monocyte	NA	
eye	monocyte	monocyte	monocyte	NA	
large_intestine	monocyte	monocyte	monocyte	NA	
liver	monocyte	monocyte	monocyte	NA	
lung	classical_monocyte	classical_monocyte	monocyte	classical_monocyte	
lung	non-classical_monocyte	non-classical_monocyte	monocyte	non-classical_monocyte	
lung	intermediate_monocyte	intermediate_monocyte	monocyte	intermediate_monocyte	
lymph_node	classical_monocyte	classical_monocyte	monocyte	classical_monocyte	
lymph_node	intermediate_monocyte	intermediate_monocyte	monocyte	intermediate_monocyte	
lymph_node	non-classical_monocyte	non-classical_monocyte	monocyte	non-classical_monocyte	
saliva-secreting_gland	monocyte	monocyte	monocyte	NA	
small_intestine	monocyte	monocyte	monocyte	NA	
spleen	intermediate_monocyte	intermediate_monocyte	monocyte	intermediate_monocyte	
spleen	classical_monocyte	classical_monocyte	monocyte	classical_monocyte	
thymus	monocyte	monocyte	monocyte	NA	
trachea	mucus_secreting_cell	mucus_secreting_cell	mucus_secreting_cell	NA	
heart	cardiac_muscle_cell	cardiac_muscle_cell	cardiomyocyte	NA	
skin_of_body	cell_of_skeletal_muscle	cell_of_skeletal_muscle	skeletal_muscle_cell	NA	
muscle_tissue	fast_muscle_cell	fast_muscle_cell	muscle_cell	fast_muscle_cell	
muscle_tissue	slow_muscle_cell	slow_muscle_cell	muscle_cell	slow_muscle_cell	
skin_of_body	muscle_cell	muscle_cell	muscle_cell	NA	
thymus	fast_muscle_cell	fast_muscle_cell	muscle_cell	fast_muscle_cell	
tongue	tongue_muscle_cell	tongue_muscle_cell	muscle_cell	NA	
uterus	myometrial_cell	myometrial_cell	myometrial_cell	NA	
lung	adventitial_cell	adventitial_cell	adventitial_cell	NA	
saliva-secreting_gland	adventitial_cell	adventitial_cell	adventitial_cell	NA	
heart	hepatocyte	hepatocyte	NA	NA	
adipose_tissue	leukocyte	leucocyte	NA	NA	
eye	erythroid_lineage_cell	erythroid_lineage_cell	NA	NA	
lymph_node	innate_lymphoid_cell	innate_lymphoid_cell	NA	NA	
pancreas	myeloid_cell	myeloid_cell	NA	NA	
prostate_gland	myeloid_cell	myeloid_cell	NA	NA	
spleen	innate_lymphoid_cell	innate_lymphoid_cell	NA	NA	
thymus	innate_lymphoid_cell	innate_lymphoid_cell	NA	NA	
tongue	leukocyte	immune_cell	NA	NA	
trachea	connective_tissue_cell	connective_tissue_cell	NA	NA	
uterus	leukocyte	immune_cell	NA	NA	
thymus	immature_natural_killer_cell	immature_natural_killer_cell	natural_killer_cell	immature_natural_killer_cell	
eye	retinal_bipolar_neuron	retinal_bipolar_neuron	neuron	retinal_bipolar_neuron	
eye	retinal_ganglion_cell	retinal_ganglion_cell	neuron	retinal_ganglion_cell	
adipose_tissue	neutrophil	neutrophil	neutrophil	NA	
blood	neutrophil	neutrophil	neutrophil	NA	
blood	neutrophil	cd24_neutrophil	neutrophil	cd24_neutrophil	
blood	neutrophil	nampt_neutrophil	neutrophil	nampt_neutrophil	
bone_marrow	neutrophil	nampt_neutrophil	neutrophil	nampt_neutrophil	
bone_marrow	neutrophil	cd24_neutrophil	neutrophil	cd24_neutrophil	
bone_marrow	neutrophil	neutrophil	neutrophil	NA	
large_intestine	neutrophil	neutrophil	neutrophil	NA	
liver	neutrophil	neutrophil	neutrophil	NA	
lung	neutrophil	neutrophil	neutrophil	NA	
lymph_node	neutrophil	neutrophil	neutrophil	NA	
prostate_gland	neutrophil	neutrophil	neutrophil	NA	
saliva-secreting_gland	neutrophil	neutrophil	neutrophil	NA	
small_intestine	neutrophil	neutrophil	neutrophil	NA	
spleen	neutrophil	neutrophil	neutrophil	NA	
trachea	neutrophil	neutrophil	neutrophil	NA	
large_intestine	paneth_cell_of_colon	paneth_cell_of_epithelium_of_large_intestine	paneth_cell	NA	
small_intestine	paneth_cell_of_epithelium_of_small_intestine	paneth_cell_of_epithelium_of_small_intestine	paneth_cell	NA	
bladder_organ	pericyte_cell	pericyte_cell	pericyte	NA	
saliva-secreting_gland	pericyte_cell	pericyte_cell	pericyte	NA	
tongue	pericyte_cell	pericyte_cell	pericyte	NA	
uterus	pericyte_cell	pericyte_cell	pericyte	NA	
vasculature	pericyte_cell	pericyte_cell	pericyte	NA	
lung	pericyte_cell	pericyte_cell	pericyte	NA	
mammary_gland	pericyte_cell	pericyte_cell	pericyte	NA	
muscle_tissue	pericyte_cell	pericyte_cell	pericyte	NA	
blood	platelet	platelet	platelet	NA	
spleen	platelet	platelet	platelet	NA	
lung	type_II_pneumocyte	type_ii_pneumocyte	pneumocyte	type_ii_pneumocyte	
lung	type_I_pneumocyte	type_i_pneumocyte	pneumocyte	type_i_pneumocyte	
pancreas	pancreatic_PP_cell	pancreatic_pp_cell	pp_cell	NA	
saliva-secreting_gland	salivary_gland_cell	salivary_gland_cell	salivary_gland_cell	NA	
trachea	secretory_cell	secretory_cell	NA	NA	
lung	serous_cell_of_epithelium_of_bronchus	serous_cell_of_epithelium_of_bronchus	serous_cell	NA	
trachea	serous_cell_of_epithelium_of_trachea	serous_cell_of_epithelium_of_trachea	serous_cell	NA	
muscle_tissue	skeletal_muscle_satellite_stem_cell	skeletal_muscle_satellite_stem_cell	skeletal_muscle_satellite_stem_cell	NA	
adipose_tissue	smooth_muscle_cell	smooth_muscle_cell	smooth_muscle_cell	NA	
bladder_organ	smooth_muscle_cell	smooth_muscle_cell	smooth_muscle_cell	NA	
heart	smooth_muscle_cell	smooth_muscle_cell	smooth_muscle_cell	NA	
lung	smooth_muscle_cell	smooth_muscle_cell	smooth_muscle_cell	NA	
lung	bronchial_smooth_muscle_cell	bronchial_smooth_muscle_cell	smooth_muscle_cell	bronchial_smooth_muscle_cell	
lung	vascular_associated_smooth_muscle_cell	vascular_associated_smooth_muscle_cell	smooth_muscle_cell	vascular_associated_smooth_muscle_cell	
mammary_gland	vascular_associated_smooth_muscle_cell	vascular_associated_smooth_muscle_cell	smooth_muscle_cell	vascular_associated_smooth_muscle_cell	
muscle_tissue	smooth_muscle_cell	smooth_muscle_cell	smooth_muscle_cell	NA	
prostate_gland	smooth_muscle_cell	smooth_muscle_cell	smooth_muscle_cell	NA	
skin_of_body	smooth_muscle_cell	smooth_muscle_cell	smooth_muscle_cell	NA	
thymus	vascular_associated_smooth_muscle_cell	vascular_associated_smooth_muscle_cell	smooth_muscle_cell	vascular_associated_smooth_muscle_cell	
trachea	smooth_muscle_cell	smooth_muscle_cell	smooth_muscle_cell	NA	
uterus	vascular_associated_smooth_muscle_cell	vascular_associated_smooth_muscle_cell	smooth_muscle_cell	vascular_associated_smooth_muscle_cell	
vasculature	smooth_muscle_cell	smooth_muscle_cell	smooth_muscle_cell	NA	
eye	stromal_cell	lacrimal_gland_functional_unit_cell	lacrimal_gland_functional_unit_cell	NA	
lymph_node	stromal_cell	stromal_cell	stromal_cell	NA	
skin_of_body	stromal_cell	stromal_cell	stromal_cell	NA	
eye	surface_ectodermal_cell	ocular_surface_cell	surface_ectodermal_cell	surface_ectodermal_cell	
adipose_tissue	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
adipose_tissue	T_cell	t_cell	T_cell	NA	
bladder_organ	T_cell	t_cell	T_cell	NA	
bladder_organ	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
blood	"CD4-positive,_alpha-beta_memory_T_cell"	"cd4-positive,_alpha-beta_memory_t_cell"	T_cell	TCD4_alphabeta_memory	
blood	"CD8-positive,_alpha-beta_cytokine_secreting_effector_T_cell"	"cd8-positive,_alpha-beta_cytokine_secreting_effector_t_cell"	T_cell	TCD8_alphabeta_secretingEffector	
blood	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
blood	type_I_NK_T_cell	type_i_nk_t_cell	T_cell	type_I_NKT_cell	
blood	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
blood	alpha-beta_T_cell	alpha-beta_t_cell	T_cell	T_alphabeta	
blood	"naive_thymus-derived_CD4-positive,_alpha-beta_T_cell"	"naive_thymus-derived_cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta_naive	
blood	"CD4-positive,_alpha-beta_T_cell"	"cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta_memory	
bone_marrow	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
bone_marrow	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
bone_marrow	"CD4-positive,_alpha-beta_T_cell"	"cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta	
eye	T_cell	t_cell	T_cell	NA	
eye	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
eye	"CD4-positive,_alpha-beta_T_cell"	"cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta	
kidney	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
kidney	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
kidney	CD4-positive_helper_T_cell	cd4-positive_helper_t_cell	T_cell	TCD4_helper	
large_intestine	"CD4-positive,_alpha-beta_T_cell"	"cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta	
large_intestine	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
liver	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
liver	T_cell	t_cell	T_cell	NA	
lung	"effector_CD4-positive,_alpha-beta_T_cell"	cd4-positive_alpha-beta_t_cell	T_cell	TCD4_alphabeta	
lung	"effector_CD8-positive,_alpha-beta_T_cell"	cd8-positive_alpha-beta_t_cell	T_cell	TCD8_alphabeta	
lung	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
lung	"CD4-positive,_alpha-beta_T_cell"	"cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta	
lung	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
lymph_node	"effector_CD8-positive,_alpha-beta_T_cell"	cd8-positive_alpha-beta_t_cell	T_cell	TCD8_alphabeta	
lymph_node	T_cell	t_cell	T_cell	NA	
lymph_node	type_I_NK_T_cell	type_i_nk_t_cell	T_cell	type_I_NKT_cell	
lymph_node	"effector_CD4-positive,_alpha-beta_T_cell"	cd4-positive_alpha-beta_t_cell	T_cell	TCD4_alphabeta	
lymph_node	regulatory_T_cell	regulatory_t_cell	T_cell	regulatory_t_cell	
lymph_node	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
lymph_node	"naive_thymus-derived_CD4-positive,_alpha-beta_T_cell"	"naive_thymus-derived_cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta_naive	
lymph_node	"CD4-positive,_alpha-beta_memory_T_cell"	"cd4-positive,_alpha-beta_memory_t_cell"	T_cell	TCD4_alphabeta_memory	
lymph_node	"CD8-positive,_alpha-beta_memory_T_cell"	"cd8-positive,_alpha-beta_memory_t_cell"	T_cell	TCD8_alphabeta_memory	
lymph_node	mature_NK_T_cell	mature_nk_t_cell	T_cell	mature_NK_T_cell	
mammary_gland	T_cell	t_cell	T_cell	NA	
mammary_gland	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
muscle_tissue	"CD4-positive,_alpha-beta_T_cell"	"cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta	
muscle_tissue	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
muscle_tissue	mature_NK_T_cell	mature_nk_t_cell	T_cell	mature_NK_T_cell	
muscle_tissue	T_cell	t_cell	T_cell	NA	
pancreas	T_cell	t_cell	T_cell	NA	
pancreas	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
prostate_gland	mature_NK_T_cell	nkt_cell	T_cell	mature_NK_T_cell	
prostate_gland	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
prostate_gland	mature_NK_T_cell	cd8b-positive_nk_t_cell	T_cell	mature_NK_T_cell	
prostate_gland	T_cell	t_cell	T_cell	NA	
saliva-secreting_gland	CD4-positive_helper_T_cell	cd4-positive_helper_t_cell	T_cell	TCD4_helper	
saliva-secreting_gland	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
saliva-secreting_gland	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
saliva-secreting_gland	T_cell	t_cell	T_cell	NA	
skin_of_body	"CD8-positive,_alpha-beta_memory_T_cell"	"cd8-positive,_alpha-beta_memory_t_cell"	T_cell	TCD8_alphabeta	
skin_of_body	T_cell	t_cell	T_cell	NA	
skin_of_body	mature_NK_T_cell	nkt_cell	T_cell	mature_NK_T_cell	
skin_of_body	"CD8-positive,_alpha-beta_cytotoxic_T_cell"	"cd8-positive,_alpha-beta_cytotoxic_t_cell"	T_cell	TCD8_alphabeta_cytotoxic	
skin_of_body	"CD4-positive,_alpha-beta_memory_T_cell"	"cd4-positive,_alpha-beta_memory_t_cell"	T_cell	TCD4_alphabeta_memory	
skin_of_body	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
skin_of_body	"naive_thymus-derived_CD8-positive,_alpha-beta_T_cell"	"naive_thymus-derived_cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
skin_of_body	"naive_thymus-derived_CD4-positive,_alpha-beta_T_cell"	"naive_thymus-derived_cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta	
skin_of_body	regulatory_T_cell	regulatory_t_cell	T_cell	regulatory_t_cell	
skin_of_body	CD4-positive_helper_T_cell	cd4-positive_helper_t_cells	T_cell	TCD4_helper	
small_intestine	"CD4-positive,_alpha-beta_T_cell"	"cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta	
small_intestine	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
spleen	"CD8-positive,_alpha-beta_memory_T_cell"	"cd8-positive,_alpha-beta_memory_t_cell"	T_cell	TCD8_alphabeta_memory	
spleen	"naive_thymus-derived_CD4-positive,_alpha-beta_T_cell"	"naive_thymus-derived_cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta	
spleen	"CD4-positive,_alpha-beta_memory_T_cell"	"cd4-positive,_alpha-beta_memory_t_cell"	T_cell	TCD4_alphabeta_memory	
spleen	type_I_NK_T_cell	type_i_nk_t_cell	T_cell	type_I_NKT_cell	
spleen	"naive_thymus-derived_CD8-positive,_alpha-beta_T_cell"	"naive_thymus-derived_cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
spleen	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
spleen	regulatory_T_cell	regulatory_t_cell	T_cell	regulatory_t_cell	
spleen	mature_NK_T_cell	mature_nk_t_cell	T_cell	mature_NK_T_cell	
spleen	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
thymus	CD4-positive_helper_T_cell	cd4-positive_helper_t_cell	T_cell	TCD4_helper	
thymus	naive_regulatory_T_cell	naive_regulatory_t_cell	T_cell	T_regulatory_naive	
thymus	T_follicular_helper_cell	t_follicular_helper_cell	T_cell	T_helper_follicular	
thymus	"CD8-positive,_alpha-beta_cytotoxic_T_cell"	"cd8-positive,_alpha-beta_cytotoxic_t_cell"	T_cell	TCD8_alphabeta_cytotoxic	
thymus	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
thymus	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
thymus	mature_NK_T_cell	mature_nk_t_cell	T_cell	mature_NK_T_cell	
trachea	T_cell	t_cell	T_cell	NA	
trachea	"CD8-positive,_alpha-beta_T_cell"	"cd8-positive,_alpha-beta_t_cell"	T_cell	TCD8_alphabeta	
trachea	"CD4-positive,_alpha-beta_T_cell"	"cd4-positive,_alpha-beta_t_cell"	T_cell	TCD4_alphabeta	
uterus	T_cell	t_cell	T_cell	NA	
uterus	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
vasculature	T_cell	t_cell	T_cell	NA	
vasculature	mature_NK_T_cell	nk_cell	T_cell	mature_NK_T_cell	
thymus	DN4_thymocyte	dn4_thymocyte	thymocyte	DN4_thymocyte	
thymus	DN3_thymocyte	dn3_thymocyte	thymocyte	DN3_thymocyte	
thymus	DN1_thymic_pro-T_cell	dn1_thymic_pro-t_cell	thymocyte	DN1_thymic_pro-T_cell	
thymus	thymocyte	thymocyte	thymocyte	NA	
trachea	"double-positive,_alpha-beta_thymocyte"	"double-positive,_alpha-beta_thymocyte"	thymocyte	double_positive_alphabeta_thymocyte	
large_intestine	transit_amplifying_cell_of_colon	transit_amplifying_cell_of_large_intestine	transit_amplifying_cell	NA	
small_intestine	transit_amplifying_cell_of_small_intestine	transit_amplifying_cell_of_small_intestine	transit_amplifying_cell	NA	
large_intestine	intestinal_tuft_cell	intestinal_tuft_cell	tuft_cell	NA	
small_intestine	intestinal_tuft_cell	intestinal_tuft_cell	tuft_cell	NA	
bladder_organ	bladder_urothelial_cell	bladder_urothelial_cell	urothelial_cell	NA	