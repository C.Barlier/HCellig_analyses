options(stringsAsFactors = F)

suppressPackageStartupMessages({
  library(data.table)
  library(stringr)
  require(foreach)
  require(parallel)
  require(doParallel)
  require(reshape2)
  require(tictoc)
  require(progressr)
  require(scuttle)
  require(Matrix)
  require(sctransform)
  require(LaplacesDemon)
})

#run scGLQ
run_scGLQ <- function(mat,meta,resName){
  source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/scGLQ.R")
  source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/internalFunctions.R")
  source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/sctransform_libraries.R")
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/")
  d = "/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ"
  scGLQ(data = mat,metadata = meta,dir = d,file.name = resName,discretize = T,sig.frames = T,ncores=20)
  return(NULL)
}

#Load the Tabula Sapiens data
ts <- readRDS("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/DATA/tabula_sapiens/UMI10x_downsampled_tabula_sapiens.rds")
ts_mtx <- ts$mtx
ts_meta <- ts$meta
rm(ts)

#Load the Phenotype metadata obtained with getPhenotypicBacksInfo.R - subtypes for which we have data at least in two different tissues
ts_meta_pheno <- readRDS("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/DATA/tabula_sapiens/metadataPhenotypeHuman.rds")
#Filter to keep the same subtypes
ts_meta <- ts_meta[which(ts_meta$subCT %in% unique(ts_meta_pheno$subCT)),]
#Remove NAs if any
ts_meta <- ts_meta[!is.na(ts_meta$subCT),]

## Phenotypic level

#Run scGLQ backgrounds compilation for each cell subtype across different tissuess
subct <- unique(ts_meta$subCT)
subct <- subct[!is.na(subct)]
print(subct)

#Run scGLQ background compilation for each cell subtype
for(i in seq(1,length(subct))){
	ts_meta_subct <- ts_meta[which(ts_meta$subCT == subct[i]),]
  #Create phenotype column: tissue_subCT
  ts_meta_subct$pheno <- paste(ts_meta_subct$Tissue,ts_meta_subct$subCT,sep="_")
	ts_meta_subct <- ts_meta_subct[,c("Cells","pheno")]
	ts_mtx_subct <- ts_mtx[,which(dimnames(ts_mtx)[[2]] %in% ts_meta_subct$Cells)]
	run_scGLQ(ts_mtx_subct,ts_meta_subct,subct[i])
}
