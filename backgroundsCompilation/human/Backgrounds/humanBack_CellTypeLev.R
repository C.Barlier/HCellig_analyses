options(stringsAsFactors = F)

suppressPackageStartupMessages({
  library(data.table)
  library(stringr)
  require(foreach)
  require(parallel)
  require(doParallel)
  require(reshape2)
  require(tictoc)
  require(progressr)
  require(scuttle)
  require(Matrix)
})

#run scGLQ
run_scGLQ <- function(mat,meta,resName){
  source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/scGLQ.R")
  source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/internalFunctions.R")
  source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/sctransform_libraries.R")
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/")
  d = "/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ"
  scGLQ(data = mat,metadata = meta,dir = d,file.name = resName,discretize = F,sig.frames = F,ncores=60)
  return(NULL)
}

#Load the Tabula Sapiens data
ts <- readRDS("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/DATA/tabula_sapiens/UMI10x_downsampled_tabula_sapiens.rds")
ts_mtx <- ts$mtx
ts_meta <- ts$meta
rm(ts)

## Cell type level

#To gain on the running time: remove CT-Tissue for which there is less than 50 cells (are filtered out by the algorithm)
ts_meta$CTtis <- paste(ts_meta$Tissue,ts_meta$CT,sep="_")
dfFreq <- as.data.frame(table(ts_meta$CTtis))
dfFreq <- dfFreq[which(dfFreq$Freq >= 50),]
ts_meta <- ts_meta[which(ts_meta$CTtis %in% dfFreq$Var1),]

#Run the background creation with the CT and tissue information (2 layers of sampling)
ts_meta <- ts_meta[,c("Cells","Tissue","CT")]
ts_mtx <- ts_mtx[,which(dimnames(ts_mtx)[[2]] %in% ts_meta$Cells)]
run_scGLQ(ts_mtx,ts_meta,"Human")


