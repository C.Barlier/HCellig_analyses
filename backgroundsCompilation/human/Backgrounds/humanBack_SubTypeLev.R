options(stringsAsFactors = F)

suppressPackageStartupMessages({
  library(data.table)
  library(stringr)
  require(foreach)
  require(parallel)
  require(doParallel)
  require(reshape2)
  require(tictoc)
  require(progressr)
  require(scuttle)
  require(Matrix)
  require(sctransform)
  require(LaplacesDemon)
})

#scGLQ scripts
source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/scGLQ.R")
source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/internalFunctions.R")
source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/sctransform_libraries.R")

#run scGLQ backgrounds
run_scGLQ_backgrounds <- function(mat,meta,resName){
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/")
  d = "/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ"
  scGLQ(data = mat,metadata = meta,dir = d,file.name = resName,discretize = F,sig.frames = F,ncores=10)
  return(NULL)
}

#run scGLQ gene quantification and key identity genes
run_scGLQ_queries <- function(mat,meta,resName){
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ/")
  d = "/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/UMI/scGLQ"
  scGLQ(data = mat,metadata = meta,dir = d,file.name = resName,discretize = T,sig.frames = T,ncores=2)
  return(NULL)
}

#Load the Tabula Sapiens data
ts <- readRDS("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/DATA/tabula_sapiens/UMI10x_downsampled_tabula_sapiens.rds")
ts_mtx <- ts$mtx
ts_meta <- ts$meta
rm(ts)

## Cell subtypes level

## Based on manual curation (Table S2)
ct <- c("B_cell","dendritic_cell","endothelial_cell","fibroblast","monocyte","muscle_cell","pneumocyte","T_cell","thymocyte")

# Subtypes to keep (Table S2) - same complexity 
subct <- c("memory_b_cell","plasma_cell",
           "CD141_myeloid_dendritic_cell","CD1C_myeloid_dendritic_cell","plasmacytoid_dendritic_cell","myeloid_dendritic_cell", #CD141 & CD1C will be merged as myeloid dendritic cells
           "capillary_endothelial_cell","endothelial_cell_of_artery","lymphatic_endothelial","vein_endothelial_cell",
           "keratocyte","myofibroblast_cell",
           "classical_monocyte","intermediate_monocyte","non-classical_monocyte",
           "fast_muscle_cell","slow_muscle_cell",
           "type_i_pneumocyte","type_ii_pneumocyte",
           "mature_NK_T_cell","regulatory_t_cell","TCD4_alphabeta_memory","TCD4_helper","TCD8_alphabeta_cytotoxic","TCD8_alphabeta_memory",
           "DN1_thymic_pro-T_cell","DN3_thymocyte")

# 1/ Run Background compilation
for(i in seq(1,length(ct))){
	ts_meta_ct <- ts_meta[which(ts_meta$CT == ct[i]),]
  #Keep subtypes (selected based on Table S1)
	ts_meta_ct <- ts_meta_ct[,c("Cells","subCT")]
  ts_meta_ct <- ts_meta_ct[which(ts_meta_ct$subCT %in% subct),]
  #If dendritic cells: merge CD141_myeloid_dendritic_cell, CD1C_myeloid_dendritic_cell, myeloid_dendritic_cell as myeloid_dendritic_cell
  if(ct[i] == "dendritic_cell"){
    ts_meta_ct$subCT[which(ts_meta_ct$subCT == "CD141_myeloid_dendritic_cell")] <- "myeloid_dendritic_cell"
    ts_meta_ct$subCT[which(ts_meta_ct$subCT == "CD1C_myeloid_dendritic_cell")] <- "myeloid_dendritic_cell"
  }
  #Filter matrix
	ts_mtx_ct <- ts_mtx[,which(dimnames(ts_mtx)[[2]] %in% ts_meta_ct$Cells)]
  #Run backgrounds
	run_scGLQ_backgrounds(ts_mtx_ct,ts_meta_ct,ct[i])
}

# 2/ Run subtypes gene quantification and key identity genes
for(i in seq(1,length(ct))){
  #Select cell type
  ts_meta_ct <- ts_meta[which(ts_meta$CT == ct[i]),]
  #Keep subtypes (no filter)
  ts_meta_ct <- ts_meta_ct[,c("Cells","subCT")]
  #Filter matrix
  ts_mtx_ct <- ts_mtx[,which(colnames(ts_mtx) %in% ts_meta_ct$Cells)]
  #Run gene quantification and key identity genes
  run_scGLQ_queries(ts_mtx_ct,ts_meta_ct,ct[i])
}

sessionInfo() %>% capture.output(file="session_info.txt")
