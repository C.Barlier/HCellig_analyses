#!/bin/bash -l
###SBATCH --job-name=Bench
###SBATCH --mail-type=end,fail
#SBATCH --mem=1200GB
#SBATCH -N 1
#SBATCH -c 60
#SBATCH --ntasks-per-node=1
#SBATCH --time=10-00:00:00
#SBATCH --partition=bigmem
#SBATCH --qos=long

conda activate r4-base-clone1

Rscript mouseBack_CellTypeLev.R

conda deactivate
