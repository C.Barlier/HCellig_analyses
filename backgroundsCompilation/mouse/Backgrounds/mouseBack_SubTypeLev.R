options(stringsAsFactors = F)

suppressPackageStartupMessages({
  library(data.table)
  library(stringr)
  require(foreach)
  require(parallel)
  require(doParallel)
  require(reshape2)
  require(tictoc)
  require(progressr)
  require(scuttle)
  require(Matrix)
  require(sctransform)
  require(LaplacesDemon)
})

#scGLQ scripts
source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ/scGLQ.R")
source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ/internalFunctions.R")
source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ/sctransform_libraries.R")

#run scGLQ backgrounds
run_scGLQ_backgrounds <- function(mat,meta,resName){
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ/")
  d = "/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ"
  scGLQ(data = mat,metadata = meta,dir = d,org = "mouse",file.name = resName,discretize = F,sig.frames = F,ncores=10)
  return(NULL)
}

#run scGLQ gene quantification and key identity genes
run_scGLQ_queries <- function(mat,meta,resName){
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ/")
  d = "/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ"
  scGLQ(data = mat,metadata = meta,dir = d,org = "mouse",file.name = resName,discretize = T,sig.frames = T,ncores=2)
  return(NULL)
}

#Load the Tabula Muris data
tbm <- readRDS("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/manualCuration_TBM.rds")
tbm_mtx <- tbm$counts
tbm_meta <- tbm$metadata
rm(tbm)

## Cell subtype level

## Based on manual curation (Table S1) - subtypes backgrounds will be constructed for (1) mesenchymal_stem_cell and (2) T_cell
ct <- c("mesenchymal_stem_cell","T_cell")

# Subtypes to keep (Table S1) - same complexity 
subct <- c("mesenchymal_cell_Car3+","mesenchymal_cell_Scara5+",
           "T_cell_CD4+","T_cell_CD8+")

# 1/ Run Background compilation
for(i in seq(1,length(ct))){
  #Select cell type
	tbm_meta_ct <- tbm_meta[which(tbm_meta$CT == ct[i]),]
  #Keep subtypes (selected based on Table S1)
	tbm_meta_ct <- tbm_meta_ct[,c("cell.name","subCT")]
  tbm_meta_ct <- tbm_meta_ct[which(tbm_meta_ct$subCT %in% subct),]
  #Filter matrix
	tbm_mtx_ct <- tbm_mtx[,which(colnames(tbm_mtx) %in% tbm_meta_ct$cell.name)]
  #Run backgrounds
	run_scGLQ_backgrounds(tbm_mtx_ct,tbm_meta_ct,ct[i])
}

# 2/ Run subtypes gene quantification and key identity genes
for(i in seq(1,length(ct))){
  #Select cell type
  tbm_meta_ct <- tbm_meta[which(tbm_meta$CT == ct[i]),]
  #Keep subtypes (no filter)
  tbm_meta_ct <- tbm_meta_ct[,c("cell.name","subCT")]
  #Filter matrix
  tbm_mtx_ct <- tbm_mtx[,which(colnames(tbm_mtx) %in% tbm_meta_ct$cell.name)]
  #Run gene quantification and key identity genes
  run_scGLQ_queries(tbm_mtx_ct,tbm_meta_ct,ct[i])
}

sessionInfo() %>% capture.output(file="session_info.txt")