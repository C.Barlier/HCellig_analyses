#!/bin/bash -l
###SBATCH --job-name=Bench
###SBATCH --mail-type=end,fail
#SBATCH --mem=100GB
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --ntasks-per-node=1
#SBATCH --time=2-00:00:00
#SBATCH --partition=batch

conda activate r4-base-clone1

Rscript mouseBack_CellTypeLev_discretize.R

conda deactivate
