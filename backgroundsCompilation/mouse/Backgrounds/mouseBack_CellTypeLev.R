options(stringsAsFactors = F)

suppressPackageStartupMessages({
  library(data.table)
  library(stringr)
  require(foreach)
  require(parallel)
  require(doParallel)
  require(reshape2)
  require(tictoc)
  require(progressr)
  require(scuttle)
  require(Matrix)
})

#run scGLQ
run_scGLQ <- function(mat,meta,resName){
  source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ/scGLQ.R")
  source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ/internalFunctions.R")
  source("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ/sctransform_libraries.R")
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ/")
  d = "/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/scGLQ"
  scGLQ(data = mat,metadata = meta,dir = d,org = "mouse",file.name = resName,discretize = F,sig.frames = F,ncores=60)
  return(NULL)
}

#Load the Tabula Muris data
tbm <- readRDS("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/MOUSE/UMI/manualCuration_TBM.rds")
tbm_mtx <- tbm$counts
tbm_mtx <- as.matrix(tbm_mtx)
tbm_meta <- tbm$metadata
rm(tbm)

## Cell type level

#To gain on the running time: remove CT-Tissue for which there is less than 50 cells (are filtered out by the algorithm)
tbm_meta$CTtis <- paste(tbm_meta$Tissue,tbm_meta$CT,sep="_")
dfFreq <- as.data.frame(table(tbm_meta$CTtis))
dfFreq <- dfFreq[which(dfFreq$Freq >= 50),]
tbm_meta <- tbm_meta[which(tbm_meta$CTtis %in% dfFreq$Var1),]

#Run the background creation with the CT and tissue information (2 layers of sampling)
tbm_meta <- tbm_meta[,c("cell.name","Tissue","CT")]
tbm_mtx <- tbm_mtx[,which(colnames(tbm_mtx) %in% tbm_meta$cell.name)]
run_scGLQ(tbm_mtx,tbm_meta,"Mouse")


