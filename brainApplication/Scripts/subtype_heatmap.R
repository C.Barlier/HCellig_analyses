
## Importing libraries

suppressPackageStartupMessages({
  require(ggvenn)
  require(VennDiagram)
  require(ggVennDiagram)
  require(rlist)
  require(pheatmap)
  require(ggplot2)
})

## Reading significant genes from neuron subtypes  

subtype.files <- list.files(path = "DIR_TO_NEURON_SUBTYPES_SIG_FRAMES")
subtype.nm <- gsub(x = subtype.files,pattern = "_sig_tbl.txt",replacement = "")


subtype.data <- lapply(X = subtype.nm,FUN = function(nm){
  
  sig.tbl <- read.table(file = paste0("DIR_TO_NEURON_SUBTYPES_SIG_FRAMES",nm,"_sig_tbl.txt"),
                        sep = "\t",header = T,stringsAsFactors = F)
  sig.tbl <- sig.tbl[which(sig.tbl$expr.level %in% c("high","medium","low")),]
  return(sig.tbl[,c(1,7)])
})
subtype.data <- setNames(object = subtype.data,nm = subtype.nm)

## Generate subtype combinations

subtype.combn <- as.data.frame(combn(x = names(subtype.data),m = 2))

## Discretized level overlaps heatmap of subtype combinations

lapply(X = 1:ncol(subtype.combn),FUN = function(i){
  
  combn <- subtype.combn[,i]
  
  data.1 <- subtype.data[[combn[1]]]
  data.2 <- subtype.data[[combn[2]]]
  
  
  out <- as.data.frame(do.call("rbind",lapply(X = c("low","medium","high"),FUN = function(lvl.1){
    
    temp <- do.call("rbind",lapply(X = c("low","medium","high"),FUN = function(lvl.2){
     
      data.1 <- data.1[which(data.1$expr.level == lvl.1),]
      data.2 <- data.2[which(data.2$expr.level == lvl.2),]
      
      gene.overlap <- length(intersect(data.1$gene,data.2$gene))
      genes <- intersect(data.1$gene,data.2$gene)[1:5]
      genes <- genes[!is.na(genes)]
      genes <- paste0(paste0(genes,collapse = "\n"),"\n(",gene.overlap,")")
        
      row <- cbind(lvl.1,lvl.2,gene.overlap,genes)
      return(row)
    }))
  })))
  
  out$gene.overlap <- as.numeric(out$gene.overlap)
  out$lvl.1 <- factor(x = out$lvl.1,levels = c("low","medium","high"))
  out$lvl.2 <- factor(x = out$lvl.2,levels = c("low","medium","high"))
  
  p <- ggplot(data = out,mapping = aes(x = lvl.1,y = lvl.2,fill = gene.overlap)) + geom_tile() +
    scale_fill_distiller(palette = "Spectral") + geom_text(aes(label = genes)) +  xlab(combn[1]) + ylab(combn[2])
  
  ggsave(plot = p,paste0("DIR_TO_SAVE_NEURON_SUBTYPES_HEATMAPS",combn[1],"-",combn[2],".tiff"),
         device = "tiff",dpi = 300,width = 8,height = 7)
  
})
