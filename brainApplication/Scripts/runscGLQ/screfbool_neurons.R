
## Importing libraries to be used in the analysis. 

suppressPackageStartupMessages({
  require(dplyr)
  require(Matrix)
  require(foreach)
  require(parallel)
  require(doParallel)
  require(reshape2)
  require(tictoc)
  require(SingleCellExperiment)
  require(Matrix)
  require(scater)
  require(stringr)
  require(qlcMatrix)
  require(sctransform)
  require(org.Mm.eg.db)
  require(org.Hs.eg.db)
  require(scuttle)
  require(LaplacesDemon)
})


setwd("DIR_TO_scGLQ_FUNCTIONS")

source("scRefBool.R")
source("internalFunctions.R")
source("sctransform_libraries.R")

setwd("DIR_TO_COUNTS_DATA_AND_METADTA")

#Load the Brain data
brain.mtx <- readRDS("downsampled_BrainMtx.rds")
brain.meta <- readRDS("downsampled_BrainMetadata.rds")

## Subsetting Neurons metadata

neuron.meta <- brain.meta[which(brain.meta$Class == "Neuron"),c(1,4,2)]
row.names(neuron.meta) <- 1:nrow(neuron.meta)

types <- c("glutamatergic","GABAergic","glycinergic","serotoninergic",
           "dopaminergic","Motor","Sensory")

neuron.meta$type <- unlist(lapply(X = neuron.meta$Subclass,FUN = function(sub){
  
  spl <- unlist(strsplit(x = sub,split = " "))
  overlap <- intersect(spl,types)
  if(length(overlap) == 1){
    return(overlap)
  }else{
    return(NA)
  }
}))

neuron.meta <- neuron.meta[which(!is.na(neuron.meta$type)),]

## Subsetting Neurons counts metadata

counts <- brain.mtx[,which(colnames(brain.mtx) %in% neuron.meta$CellID)]
rm(brain.mtx)

## Adding column with celltype as - Neuron

neuron.meta$cell.type <- "Neuron"
neuron.meta <- neuron.meta[,c(1,5)]

## Performing check and removing lowly expressed genes

bin.counts <- counts
bin.counts[bin.counts > 0] <- 1

r.sum <- rowSums(bin.counts)
r.sum <- r.sum[which(r.sum > 0.01*ncol(bin.counts))]
counts <- counts[which(row.names(counts) %in% names(r.sum)),]

colnames(neuron.meta) <- c("cell.id","cell.type") 

invisible(gc())

## Runing scGLQ

scGLQ(data = counts,metadata = neuron.meta,org = "mouse",dir = "/mnt/irisgpfs/users/ksingh/Work/Brain_development_v4/",
          precBack = "/mnt/irisgpfs/users/ksingh/Work/Brain_development_v4/ScRbRef_Mouse/",
          file.name = "Mouse",discretize = T,sig.frames = T,ncores = 1)
