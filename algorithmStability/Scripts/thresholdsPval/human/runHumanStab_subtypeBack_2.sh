#!/bin/bash -l
###SBATCH --job-name=Bench
###SBATCH --mail-type=end,fail
#SBATCH --mem=90GB
#SBATCH -N 1
#SBATCH -c 10
#SBATCH --ntasks-per-node=1
#SBATCH --time=2-00:00:00
#SBATCH --partition=batch

conda activate r4-base-clone1

Rscript stabilityHuman_subtypeBack_2.R

conda deactivate
