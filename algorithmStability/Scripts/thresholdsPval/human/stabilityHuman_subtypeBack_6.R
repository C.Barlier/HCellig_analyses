options(stringsAsFactors = F)

suppressPackageStartupMessages({
  library(data.table)
  library(stringr)
  require(foreach)
  require(parallel)
  require(doParallel)
  require(reshape2)
  require(tictoc)
  require(progressr)
  library(stringr)
  require(scuttle)
  require(Matrix)
  library(corrplot)
  library(gt)
})
#Stability check functions
source("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/stabilityCheckFunctions.R")
#scGLQ code
source("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ/scGLQ.R")
source("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ/internalFunctions.R")
source("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ/sctransform_libraries.R")

#Checking scGLQ stability inter-runs

#run scGLQ backgrounds
run_scRB_backgrounds <- function(mat,meta,resName,fixseed){
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ/")
  d = "/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ"
  scGLQ(data = mat,metadata = meta,dir = d,file.name = resName,discretize = F,sig.frames = F,ncores=10,fixseed=fixseed)
  return(NULL)
}

#run scGLQ gene quantification and key identity genes
run_scRB_queries <- function(mat,meta,resName,fixseed){
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ/")
  d = "/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ"
  scGLQ(data = mat,metadata = meta,dir = d,file.name = resName,discretize = T,sig.frames = T,ncores=2,fixseed=fixseed)
  return(NULL)
}

#Method: run scGLQ 10 times on the same dataset & verify the p-values remain the same
#Load the Tabula Sapiens data
ts <- readRDS("/scratch/users/cbarlier/scGLQ/ANALYSES/BACKGROUNDS/HUMAN/DATA/tabula_sapiens/UMI10x_downsampled_tabula_sapiens.rds")
ts_mtx <- ts$mtx
ts_meta <- ts$meta
rm(ts)

## Cell subtype level

## Based on manual curation (Table S2)
ctv <- c("B_cell","dendritic_cell","endothelial_cell","fibroblast","monocyte","muscle_cell","pneumocyte","T_cell","thymocyte")

# Subtypes to keep (Table S2) - same complexity 
subct <- c("memory_b_cell","plasma_cell",
           "CD141_myeloid_dendritic_cell","CD1C_myeloid_dendritic_cell","plasmacytoid_dendritic_cell","myeloid_dendritic_cell", #CD141 & CD1C will be merged as myeloid dendritic cells
           "capillary_endothelial_cell","endothelial_cell_of_artery","lymphatic_endothelial","vein_endothelial_cell",
           "keratocyte","myofibroblast_cell",
           "classical_monocyte","intermediate_monocyte","non-classical_monocyte",
           "fast_muscle_cell","slow_muscle_cell",
           "type_i_pneumocyte","type_ii_pneumocyte",
           "mature_NK_T_cell","regulatory_t_cell","TCD4_alphabeta_memory","TCD4_helper","TCD8_alphabeta_cytotoxic","TCD8_alphabeta_memory",
           "DN1_thymic_pro-T_cell","DN3_thymocyte")

#Select metadata accordingly
ts_meta <- ts_meta[which(ts_meta$CT == ctv[6]),]
ts_meta <- ts_meta[,c("Cells","subCT")]
ts_meta <- ts_meta[!is.na(ts_meta$subCT),]

#Filter mtx
ts_mtx<- ts_mtx[,which(dimnames(ts_mtx)[[2]] %in% ts_meta$Cells)]

#Seeds
seedvec <- c(1234,2345,3456,4567,5678,6789,7890,1357,2468,3579)

#Run scGLQ 10x
for(i in seq(1,10)){
  setwd("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ")
  #1/ Run background: filter subtypes
  ts_meta_back <- ts_meta[which(ts_meta$subCT %in% subct),]
  ts_mtx_back <- ts_mtx[,which(dimnames(ts_mtx)[[2]] %in% ts_meta_back$Cells)]
  #Compile cell subtype backgrounds
  run_scRB_backgrounds(ts_mtx_back,ts_meta_back,paste0(ctv[6],"_Run",i),seedvec[i])
  #2/ Run query discretization and key identity genes
  run_scRB_queries(ts_mtx,ts_meta,paste0(ctv[6],"_Run",i),seedvec[i])
}

#Check stability - background threshold
plotsl <- list()
thrsl <- list()
for(i in seq(1,10)){
  pathRun <- paste0("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ/Ref_",ctv[6],"_Run",i)
  fileRun <- paste(paste0(ctv[6],"_Run",i),"_th_dist.rds",sep="")
  thrsl[[i]] <- readRDS(paste(pathRun,fileRun,sep="/"))
}
#Generate plots df
plotsl <- runDiffThresholds(thrsl)
#Save
saveRDS(plotsl,paste0("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/RESULTS/",ctv[6],"_thresholds_stability.rds"))

#Get p-values of each runs & each subtype
subsct <- unique(ts_meta$subCT)

#Stability of p-values (query cell types)
ct_tables <- list()
ct_pv <- list()
#For each cell type
for(i in seq(1,length(subsct))){
  #If the cell types has been discretized
  p <- paste0("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ/Ref_",ctv[6],"_Run",1)
  if(file.exists(paste(p,paste(paste("Query",subsct[i],sep="_"),paste(subsct[i],"pvals.rds",sep="_"),sep="/"),sep="/"))){
    #For each run
    pdownl <- list()
    pupl <- list()
    for(j in seq(1,10)){
      pathRun <- paste0("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/scGLQ/Ref_",ctv[6],"_Run",j)
      rf <- readRDS(paste(pathRun,paste(paste("Query",subsct[i],sep="_"),paste(subsct[i],"pvals.rds",sep="_"),sep="/"),sep="/"))
      pdownl[[j]] <- rf$sig_nexp
      pupl[[j]] <- rf$sig_exp
    }
    ct_pv[[subsct[i]]] <- list("pdown"=pdownl,"pup"=pupl)
    ct_tables[[subsct[i]]] <- runDiffPvalues(ct_pv[[subsct[i]]][["pdown"]],ct_pv[[subsct[i]]][["pup"]],subsct[i])
  }
}

#Save p-values stability
saveRDS(ct_tables,paste0("/scratch/users/cbarlier/scGLQ/ANALYSES/STABILITY/HUMAN/RESULTS/",ctv[6],"_pvalues_stability.rds"))
