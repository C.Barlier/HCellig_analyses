# Identified biomdal genes stability across runs
# Compute the percentage of common prediction 
# Return matrix of stability for the subtype background
runStabBimod <- function(bimodl){

  #Init matrix
  mtxstab <- matrix(rep(NA,100),ncol=10,nrow=10)

  #For each comparison of runs
  for(x in seq(1,10)){
    for(y in seq(1,10)){

      #Bimodal genes R1
      bimodg1 <- bimodl[[x]]

      #Bimodal genes R2
      bimodg2 <- bimodl[[y]]

      #Score commolaity
      mtxstab[x,y] <- (length(intersect(bimodg1,bimodg2))*2)/(length(bimodg1)+length(bimodg2))
    }
  }

  return(mtxstab)
}
