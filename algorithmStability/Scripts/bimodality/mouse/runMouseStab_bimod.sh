#!/bin/bash -l
###SBATCH --job-name=Bench
###SBATCH --mail-type=end,fail
#SBATCH --mem=10GB
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=0-05:00:00
#SBATCH --partition=batch

conda activate r4-base-clone1

Rscript stabilityMouse_bimod.R

conda deactivate
