# Sig.frame stability across runs
# Compute the percentage of common prediction for the genes (low,medium and high levels)
# Return matrix of stability for the subtype
runDiffSigFrame <- function(sigfl){

  #Init matrix
  mtxstab <- matrix(rep(NA,100),ncol=10,nrow=10)

  #For each comparison of runs
  for(x in seq(1,10)){
    for(y in seq(1,10)){

      #Sig.frame R1
      sigf1 <- sigfl[[x]]
      sigf1 <- sigf1[,c("gene","expr.level")]

      #Sig.frame R2
      sigf2 <- sigfl[[y]]
      sigf2 <- sigf2[,c("gene","expr.level")]

      #Comparison - low
      scLow <- NA
      sigf1_low <- sigf1[which(sigf1$expr.level == "low"),]
      sigf2_low <- sigf2[which(sigf2$expr.level == "low"),]
      if(nrow(sigf1_low)>0 & nrow(sigf2_low)>0){
        scLow <- (length(intersect(sigf1_low$gene,sigf2_low$gene))*2)/(nrow(sigf1_low)+nrow(sigf2_low))
      }else if((nrow(sigf1_low)>0 & nrow(sigf2_low)<0) || nrow(sigf1_low)<0 & nrow(sigf2_low)>0){
        scLow <- 0 #If only one run has gene: commonality = 0
      }else{
        #Nothing to assess
        scLow <- NA
      }

      #Comparison - medium
      scMedium <- NA
      sigf1_medium <- sigf1[which(sigf1$expr.level == "medium"),]
      sigf2_medium <- sigf2[which(sigf2$expr.level == "medium"),]
      if(nrow(sigf1_medium)>0 & nrow(sigf2_medium)>0){
        scMedium <- (length(intersect(sigf1_medium$gene,sigf2_medium$gene))*2)/(nrow(sigf1_medium)+nrow(sigf2_medium))
      }else if((nrow(sigf1_medium)>0 & nrow(sigf2_medium)<0) || nrow(sigf1_medium)<0 & nrow(sigf2_medium)>0){
        scMedium <- 0 #If only one run has gene: commonality = 0
      }else{
        #Nothing to assess
        scMedium <- NA
      }

      #Comparison - high
      scHigh <- NA
      sigf1_high <- sigf1[which(sigf1$expr.level == "high"),]
      sigf2_high <- sigf2[which(sigf2$expr.level == "high"),]
      if(nrow(sigf1_high)>0 & nrow(sigf2_high)>0){
        scHigh <- (length(intersect(sigf1_high$gene,sigf2_high$gene))*2)/(nrow(sigf1_high)+nrow(sigf2_high))
      }else if((nrow(sigf1_high)>0 & nrow(sigf2_high)<0) || nrow(sigf1_high)<0 & nrow(sigf2_high)>0){
        scHigh <- 0 #If only one run has gene: commonality = 0
      }else{
        #Nothing to assess
        scHigh <- NA
      }

      vecScore <- c(scLow,scMedium,scHigh)
      vecScore <- vecScore[!is.na(vecScore)]

      if(length(vecScore)>0){
        #Add to matrix
        mtxstab[x,y] <- median(vecScore)
      }else{
        mtxstab[x,y] <- NA
      }
    }
  }

  return(mtxstab)
}
