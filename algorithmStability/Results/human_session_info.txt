R version 4.0.3 (2020-10-10)
Platform: x86_64-conda-linux-gnu (64-bit)
Running under: CentOS Linux 7 (Core)

Matrix products: default
BLAS/LAPACK: /mnt/irisgpfs/users/cbarlier/libraries/miniconda3/envs/r4-base-clone2/lib/libopenblasp-r0.3.12.so

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats4    parallel  stats     graphics  grDevices utils     datasets 
[8] methods   base     

other attached packages:
 [1] gt_0.3.1                    corrplot_0.90              
 [3] Matrix_1.3-4                scuttle_1.0.4              
 [5] SingleCellExperiment_1.12.0 SummarizedExperiment_1.20.0
 [7] Biobase_2.50.0              GenomicRanges_1.42.0       
 [9] GenomeInfoDb_1.26.7         IRanges_2.24.1             
[11] S4Vectors_0.28.1            BiocGenerics_0.36.1        
[13] MatrixGenerics_1.2.1        matrixStats_0.59.0         
[15] progressr_0.9.0             tictoc_1.0.1               
[17] reshape2_1.4.4              doParallel_1.0.16          
[19] iterators_1.0.13            foreach_1.5.1              
[21] stringr_1.4.0               data.table_1.14.0          

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.7                lattice_0.20-41          
 [3] listenv_0.8.0             assertthat_0.2.1         
 [5] digest_0.6.27             utf8_1.2.1               
 [7] slam_0.1-48               parallelly_1.26.1        
 [9] R6_2.5.0                  plyr_1.8.6               
[11] sparsesvd_0.2             qlcMatrix_0.9.7          
[13] ggplot2_3.3.5             pillar_1.6.1             
[15] sparseMatrixStats_1.2.1   zlibbioc_1.36.0          
[17] rlang_0.4.11              BiocParallel_1.24.1      
[19] RCurl_1.98-1.3            munsell_0.5.0            
[21] beachmat_2.6.4            sctransform_0.3.2        
[23] DelayedArray_0.16.3       compiler_4.0.3           
[25] pkgconfig_2.0.3           globals_0.14.0           
[27] htmltools_0.5.1.1         tidyselect_1.1.1         
[29] gridExtra_2.3             tibble_3.1.2             
[31] GenomeInfoDbData_1.2.4    codetools_0.2-18         
[33] fansi_0.5.0               future_1.21.0            
[35] crayon_1.4.1              dplyr_1.0.7              
[37] MASS_7.3-54               bitops_1.0-7             
[39] grid_4.0.3                gtable_0.3.0             
[41] lifecycle_1.0.0           DBI_1.1.1                
[43] magrittr_2.0.1            scales_1.1.1             
[45] docopt_0.7.1              future.apply_1.7.0       
[47] stringi_1.6.2             XVector_0.30.0           
[49] DelayedMatrixStats_1.12.3 ellipsis_0.3.2           
[51] generics_0.1.0            vctrs_0.3.8              
[53] tools_4.0.3               glue_1.4.2               
[55] purrr_0.3.4               colorspace_2.0-2         
