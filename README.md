# HCellig Data Analysis

This repository contains the scripts used to perform the analysis presented in the paper "Quantifying gene expression to characterize hierarchical cell identity" *(C. Barlier et al., in preparation)*

**HCellig R package : https://github.com/BarlierC/HCellig**

**Pre-compiled background datasets: https://gitlab.com/C.Barlier/HCellig_backgrounds**

**Cell identity atlases for mouse and human: https://gitlab.com/C.Barlier/HCI**
