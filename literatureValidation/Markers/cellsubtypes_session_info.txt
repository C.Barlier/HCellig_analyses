R version 4.0.5 (2021-03-31)
Platform: x86_64-apple-darwin17.0 (64-bit)
Running under: macOS Big Sur 11.6

Matrix products: default
LAPACK: /Library/Frameworks/R.framework/Versions/4.0/Resources/lib/libRlapack.dylib

locale:
[1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8

attached base packages:
[1] parallel  stats4    stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] umap_0.2.7.0                forcats_0.5.1               stringr_1.4.0               dplyr_1.0.7                
 [5] purrr_0.3.4                 readr_2.1.1                 tidyr_1.1.4                 tibble_3.1.6               
 [9] ggplot2_3.3.5               tidyverse_1.3.1             pheatmap_1.0.12             org.Mm.eg.db_3.12.0        
[13] org.Hs.eg.db_3.12.0         AnnotationDbi_1.52.0        clusterProfiler_3.18.1      data.table_1.14.2          
[17] SingleCellExperiment_1.12.0 SummarizedExperiment_1.20.0 Biobase_2.50.0              GenomicRanges_1.42.0       
[21] GenomeInfoDb_1.26.7         IRanges_2.24.1              S4Vectors_0.28.1            BiocGenerics_0.36.1        
[25] MatrixGenerics_1.2.1        matrixStats_0.61.0         

loaded via a namespace (and not attached):
  [1] fgsea_1.16.0           colorspace_2.0-2       ellipsis_0.3.2         qvalue_2.22.0          XVector_0.30.0         fs_1.5.2              
  [7] rstudioapi_0.13        farver_2.1.0           graphlayouts_0.8.0     ggrepel_0.9.1          bit64_4.0.5            RSpectra_0.16-0       
 [13] fansi_1.0.0            scatterpie_0.1.7       lubridate_1.8.0        xml2_1.3.3             splines_4.0.5          cachem_1.0.6          
 [19] GOSemSim_2.16.1        polyclip_1.10-0        jsonlite_1.7.2         broom_0.7.11           GO.db_3.12.1           dbplyr_2.1.1          
 [25] png_0.1-7              ggforce_0.3.3          BiocManager_1.30.16    compiler_4.0.5         httr_1.4.2             rvcheck_0.2.1         
 [31] backports_1.4.1        assertthat_0.2.1       Matrix_1.4-0           fastmap_1.1.0          cli_3.1.0              tweenr_1.0.2          
 [37] tools_4.0.5            igraph_1.2.11          gtable_0.3.0           glue_1.6.0             GenomeInfoDbData_1.2.4 reshape2_1.4.4        
 [43] DO.db_2.9              fastmatch_1.1-3        Rcpp_1.0.8             enrichplot_1.10.2      cellranger_1.1.0       vctrs_0.3.8           
 [49] ggraph_2.0.5           rvest_1.0.2            lifecycle_1.0.1        DOSE_3.16.0            zlibbioc_1.36.0        MASS_7.3-53.1         
 [55] scales_1.1.1           tidygraph_1.2.0        hms_1.1.1              RColorBrewer_1.1-2     reticulate_1.22        memoise_2.0.1         
 [61] gridExtra_2.3          downloader_0.4         ggfun_0.0.5            yulab.utils_0.0.4      stringi_1.7.6          RSQLite_2.2.9         
 [67] BiocParallel_1.24.1    rlang_0.4.12           pkgconfig_2.0.3        bitops_1.0-7           lattice_0.20-41        labeling_0.4.2        
 [73] cowplot_1.1.1          shadowtext_0.1.1       bit_4.0.4              tidyselect_1.1.1       plyr_1.8.6             magrittr_2.0.1        
 [79] R6_2.5.1               generics_0.1.1         DelayedArray_0.16.3    DBI_1.1.2              withr_2.4.3            pillar_1.6.4          
 [85] haven_2.4.3            RCurl_1.98-1.5         modelr_0.1.8           crayon_1.4.2           utf8_1.2.2             tzdb_0.2.0            
 [91] viridis_0.6.2          grid_4.0.5             readxl_1.3.1           blob_1.2.2             reprex_2.0.1           digest_0.6.29         
 [97] openssl_1.4.6          munsell_0.5.0          viridisLite_0.4.0      askpass_1.1           
